from flask import Flask, render_template, request, redirect, url_for
from pytube import YouTube
import os

class Downloadyt():
    def __init__(self, linkyt):
        self.link = linkyt
    def audio(self):
        try:
            self.mp3files = YouTube(self.link).streams.get_audio_only().download()
        except:
            pass
    def video(self):
        try:
            self.mp4files = YouTube(self.link).streams.filter(res="720p",progressive=True).all().download()
        except:
            pass

app = Flask(__name__)

@app.route("/")
def mainpage():
    return render_template("homepage.html")

@app.route("/youtube", methods=["GET","POST"])
def youtubepage():
    if request.method == "POST":
        path = "./"
        if request.form["submit_download"] == "Download Audio":
            link = request.form["input_link"]
            try:
                mp3files = YouTube(link).streams.get_audio_only().download(path)
            except:
                return redirect("/error")
        elif request.form["submit_download"] == "Video 1080":
            link = request.form.get("input_link")
            try:
                mp4files = YouTube(link).streams.filter(res="1080p",progressive=True).first().download()
            except:
                return redirect("/error")
        elif request.form["submit_download"] == "Video 720":
            link = request.form.get("input_link")
            try:
                mp4files = YouTube(link).streams.filter(res="720p",progressive=True).first().download()
            except:
                return redirect("/error")
        elif request.form["submit_download"] == "Video 480":
            link = request.form.get("input_link")
            try:
                mp4files = YouTube(link).streams.filter(res="480p",progressive=True).first().download()
            except:
                return redirect("/error")
        else:
            pass
    return render_template("youtube.html", title="youtube")

@app.route("/control", methods=["GET","POST"])
def controlpage():
    if request.method == "POST":
        req = request.form["time_button"]
        if req == "8" or req == "7" or req == "5":
            os.system(f"sudo su coba -c 'sudo /usr/sbin/rtcwake -m mem -s {int(request.form['time_button'])*3600}'")
        elif req == "custom":
            os.system(f"sudo su coba -c 'sudo /usr/sbin/rtcwake -m mem -s {int(float(request.form['custom_time'])*60)}'")
        else:
            pass
    else:
        pass
    return render_template("control.html")


@app.route("/error")
def errorpage():
    return render_template("error.html",title="error")